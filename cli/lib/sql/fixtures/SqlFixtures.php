<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cli\Lib\Sql\Fixtures;

use DateTime;

/**
 * Class SqlFixtures.
 */
final class SqlFixtures
{
    /**
     * @return array
     */
    public static function getFixtures(): array
    {
        $arr = [
            'page' => [
                'data' => [
                    [
                        'name' => 'home',
                        'created_at' => '2018-10-23 13:41:12',
                        'authorization_level' => null,
                        'authorization_relation' => null,
                    ],
                    [
                        'name' => 'ranking',
                        'created_at' => '2018-10-23 13:31:12',
                        'authorization_level' => null,
                        'authorization_relation' => null,
                    ],
                    [
                        'name' => 'faq',
                        'created_at' => '2018-10-19 09:17:08',
                        'authorization_level' => null,
                        'authorization_relation' => null,
                    ]
                ],
            ],
            'authorization_group' => [
                'data' => [
                    [
                        //1
                        'name' => 'ANONYMOUS_USER',
                        'uuid' => sha1(uniqid(true)),
                        'parent_group'=> null,
                    ],
                    [
                        //2
                        'name' => 'REGISTERED_USER',
                        'uuid' => sha1(uniqid(true)),
                        'parent_group'=> null,
                    ],
                    [
                        //3
                        'name' => 'PREMIUM_USER',
                        'uuid' => sha1(uniqid(true)),
                        'parent_group'=> 2, //registered user
                    ],
                    [
                        //4
                        'name' => 'GAMEMASTER_USER',
                        'uuid' => sha1(uniqid(true)),
                        'parent_group'=> 3, //premium user
                    ],
                    [
                        //5
                        'name' => 'ADMIN_USER',
                        'uuid' => sha1(uniqid(true)),
                        'parent_group'=> 4, //premium user
                    ],
                ],
            ],
            'user_password' => [
                'data' => [
                    [
                        'user_id' => 1,
                        'suffix' => md5('Dai7//"!)(EDUISDH)('),
                        'password' => hash('sha256', md5('Dai7//"!)(EDUISDH)(') . sha1('1234Test')),
                        'updated_at' => null,
                    ],
                    [
                        'user_id' => 2,
                        'suffix' => md5('OUDHAO(S=P"OJ!PJ!'),
                        'password' => hash('sha256', md5('OUDHAO(S=P"OJ!PJ!') . sha1('Test4321')),
                        'updated_at' => null,
                    ],
                    [
                        'user_id' => 3,
                        'suffix' => md5('DASIUDZ"(21hod('),
                        'password' => hash('sha256', md5('DASIUDZ"(21hod(') . sha1('adil1uo1ej8!')),
                        'updated_at' => null,
                    ],
                ],
            ],
            'user' => [
                'data' => [
                    [
                        'email' => 'xsrf@example.org',
                        'authorization_group_id' => 5,
                        'created_at' => '2018-10-19 09:17:08',
                        'updated_at' => null,
                        'password_id' => 1,
                    ],
                    [
                        'email' => 'lejla@example.org',
                        'authorization_group_id' => 4,
                        'created_at' => '2018-10-19 09:17:08',
                        'updated_at' => null,
                        'password_id' => 2,
                    ],
                    [
                        'email' => 'jan@example.org',
                        'authorization_group_id' => 4,
                        'created_at' => '2018-10-19 11:15:08',
                        'updated_at' => null,
                        'password_id' => 3,
                    ],
                ],
            ],
        ];

        return $arr;
    }
}
