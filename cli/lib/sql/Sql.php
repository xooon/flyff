<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cli\Lib;

use Cli\Lib\Sql\Builder\QueryBuilder;
use Cli\Lib\Sql\Resource\SqlResourceInterface;

/**
 * Class SqlHandler.
 */
class Sql
{
    /**
     * @var SqlResourceInterface
     */
    private $pdo;

    /**
     * Sql constructor.
     *
     * @param SqlResourceInterface $sqlResource
     */

    public function __construct(SqlResourceInterface $sqlResource)
    {
        $this->pdo = $sqlResource::createPdoInstance();
    }

    /**
     * @return QueryBuilder
     */
    public function query():QueryBuilder
    {
        return new QueryBuilder();
    }

    /**
     * @param string $query
     * @param array $params
     * @return array|null
     */
    public function execute(
        string $query,
        array $params = []
    ):?array
    {
        $stmt = $this->pdo->prepare($query);
        if($stmt->execute($params)){
            $result = $stmt->fetchAll();
            if($result !== null)
            {
                return $result;
            }
        }

        return null;
    }

}
