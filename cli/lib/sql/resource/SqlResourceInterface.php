<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cli\Lib\Sql\Resource;

use PDO;

interface SqlResourceInterface
{
    /**
     * @param array $options
     *
     * @return PDO
     */
    public static function createPdoInstance(array $options = []): PDO;
}
