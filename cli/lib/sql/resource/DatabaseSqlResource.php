<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cli\Lib\Sql\Resource;

use PDO;

/**
 * Class DatabaseSqlResource.
 */
class DatabaseSqlResource implements SqlResourceInterface
{
    const DSN = 'mysql:host=localhost;';
    const USER = 'root';
    const PASSWD = '';

    /**
     * @return PDO
     */
    public static function createPdoInstance(array $options = []): PDO
    {
        return new PDO(self::DSN, self::USER, self::PASSWD, $options);
    }
}
