<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cli\Lib\Sql\Manager;

use Cli\Lib\Sql;
use Cli\Lib\Sql\Fixtures\SqlFixtures;

/**
 * Class SqlManager.
 */
class SqlManager
{
    /**
     * @var Sql;
     */
    private $sql;

    /**
     * SqlManager constructor.
     * @param Sql $sql
     */
    public function __construct(Sql $sql)
    {
        $this->sql = $sql;
    }

    public function dropFixtures():void
    {
        $fixtures = SqlFixtures::getFixtures();
        for($i = 0;$i < 5;$i++){
            foreach($fixtures as $table => $options)
            {
                $query = sprintf('DROP TABLE %s', $table);
                $this->sql->execute($query);
                if($i === 4){
                    printf("\n\nExecuted: \033[33m%s\033[0m\n\n", $query);
                }
            }
        }
        printf("\n\n\033[32mAll tables filled with fixtures has been dropped.\033[0m\n\n");
    }

    public function clearFixtures():void
    {
        $fixtures = SqlFixtures::getFixtures();
       for($i = 0;$i < 5;$i++)
       {
           foreach($fixtures as $table => $options)
           {
               $query = sprintf('TRUNCATE %s', $table);
               $this->sql->execute($query);
               if($i === 4){
                   printf("\n\nExecuted: \033[33m%s\033[0m\n\n", $query);
               }
           }
       }
        printf("\n\n\033[32mAll tables filled with fixtures has been cleared.\033[0m\n\n");
    }

    public function loadFixtures():void
    {
        $fixtures = SqlFixtures::getFixtures();
        $this->insertFixtures($fixtures);
    }

    /**
     * @param array $fixtures
     */
    private function insertFixtures(array $fixtures = []):void
    {
        foreach($fixtures as $fixture => $options){
            $table = $fixture;
            $query = sprintf('TRUNCATE %s', $table);
            //clear table
            $this->sql->execute($query);
            //execute info
            printf("\n\nPreviously executed: \033[33m%s\033[0m\n\n", $query);

            $rowStr = '';
            $valueStr = '';

            $rowIterator = 0;
            $rowMax = sizeof($options['data'][0]);

            $query = sprintf('SELECT id FROM %s LIMIT 1', $table);
            $res = $this->sql->execute($query);
             //execute info
            printf("\n\nCheck executed: \033[33m%s\033[0m\n\n", $query);

            if(sizeof($res) > 0){
                printf("\n\nCheck executed: \033[31m%s\033[0m\n\n", 'NOT EMPTY');
            }else {
                foreach($options['data'][0] as $rowKey => $unusedValue){
                    if($rowIterator !== $rowMax-1)
                    {
                        $rowStr .= $rowKey . ', ';
                        $valueStr .= ':' . $rowKey . ', ';
                    }else {
                        $rowStr .= $rowKey;
                        $valueStr .= ':' . $rowKey;
                    }
                    $rowIterator++;
                }

                foreach($options['data'] as $data){
                    //build insert query
                    $query = sprintf('INSERT INTO %s (%s) VALUES (%s)', $table, $rowStr, $valueStr);
                    //execute info
                    printf("\n\nExecuted: \033[33m%s\033[0m\n\n", $query);
                    //execute insert query
                    $this->sql->execute($query, $data);
                }

                printf("\n\nCheck executed: \033[32m%s\033[0m\n\n", 'OK');
            }
        }
    }
}
