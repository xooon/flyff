<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

//require Sql Library
require_once 'sql/fixtures/SqlFixtures.php';
require_once 'sql/resource/SqlResourceInterface.php';
require_once 'sql/resource/DatabaseSqlResource.php';
require_once 'sql/resource/ApiSqlResource.php';
require_once 'sql/builder/QueryBuilder.php';
require_once 'sql/manager/SqlManager.php';
require_once 'sql/Sql.php';
