<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cli\Actions\Database;

use Cli\Lib\Sql;
use Cli\Lib\Sql\Manager\SqlManager;
use Cli\Lib\Sql\Resource\ApiSqlResource;

$sql = new Sql(new ApiSqlResource());
$sqlManager = new SqlManager($sql);
$sqlManager->clearFixtures();
