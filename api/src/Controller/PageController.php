<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Controller;

use App\Entity\User;
use App\Manager\Page\PageManager;
use App\Manager\User\UserManager;
use App\Test\Testabc;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * * @Route("/page/{name}", name="page")
     *
     * @param $name
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function index($name)
    {
        $pageManager = new PageManager($this->getDoctrine()->getManager());
        $page = $pageManager->get($name);

        return $this->json([
            'id' => $page->getId(),
            'name' => $page->getName(),
            'authorizationLevel' => $page->getAuthorizationLevel(),
            'authorizationRelation' => $page->getAuthorizationRelation(),
            'createdAt' => $page->getCreatedAt(),
        ]);
    }
}
