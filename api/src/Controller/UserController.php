<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Controller;

use App\Security\Authenticator;
use App\Security\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/auth", name="user_auth")
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function auth()
    {
        $authenticator = new Authenticator();
        if (!$authenticator->authenticate($this->getDoctrine()->getManager())) {
            return $this->json(['auth' => false]);
        }

        return $this->json(['auth' => true]);
    }

    /**
     * @Route("/user/clear", name="user_clear")
     */
    public function clear()
    {
        $session = new Session();
        $session->clear();

        return $this->json(['clear' => true]);
    }
}
