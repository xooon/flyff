<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $authorizationLevel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorizationRelation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getAuthorizationLevel(): ?int
    {
        return $this->authorizationLevel;
    }

    public function setAuthorizationLevel(?int $authorizationLevel): self
    {
        $this->authorizationLevel = $authorizationLevel;

        return $this;
    }

    public function getAuthorizationRelation(): ?string
    {
        return $this->authorizationRelation;
    }

    public function setAuthorizationRelation(?string $authorizationRelation): self
    {
        $this->authorizationRelation = $authorizationRelation;

        return $this;
    }
}
