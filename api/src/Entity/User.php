<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AuthorizationGroup")
     */
    private $authorizationGroup;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserPassword", inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $password;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getAuthorizationGroup(): ?AuthorizationGroup
    {
        return $this->authorizationGroup;
    }

    public function setAuthorizationGroup(?AuthorizationGroup $authorizationGroup): self
    {
        $this->authorizationGroup = $authorizationGroup;

        return $this;
    }

    public function getPassword(): ?UserPassword
    {
        return $this->password;
    }

    public function setPassword(UserPassword $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }
}
