<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\EventSubscriber\Response;

use App\Security\Session\Session;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ResponseSubscriber
 * @package App\EventSubscriber\Response
 */
class ResponseSubscriber implements EventSubscriberInterface
{
    private $session;

    /**
     * ResponseSubscriber constructor.
     */
    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {

        return array(
            KernelEvents::RESPONSE => array(
                array('onKernelResponsePre', 10),
                array('onKernelResponsePost', -10),
            ),
        );
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponsePre(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $content = json_decode($response->getContent(), true);
        $response->setContent(json_encode([
            'session' => $this->session->getSessionValues(),
            'timestamp' => date_format(date_create(),'Y.m.d H:i:s'),
            'content' => $content,
        ]));
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponsePost(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $response->setContent(json_encode(array_merge(
            json_decode($response->getContent(), true),
            ['trace' => hash('sha256', $response->getContent())]
        )));
    }
}
