<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\EventSubscriber\Session;

use App\Security\Authenticator;
use App\Security\Session\Session;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class AuthenticationSubscriber.
 */
class AuthenticationSubscriber implements EventSubscriberInterface
{

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => [
                ['onKernelController', 100],
            ],
        ];
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $authenticator = new Authenticator();
    }
}
