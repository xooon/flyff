<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Security;

use App\Manager\User\UserManager;
use App\Security\Session\Session;
use Doctrine\ORM\EntityManagerInterface;

class Authenticator
{
    private $session;

    /**
     * Authenticator constructor.
     */
    public function __construct()
    {
        $this->session = new Session();

        if (!isset($_SESSION)) {
            $this->session->set('timestamp', new \DateTime());
            $this->session->set('token', sha1(uniqid(true).time()));
        }
    }

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return null|bool
     */
    public function authenticate(EntityManagerInterface $entityManager): ?bool
    {
        $email = 'xsrf@example.org';
        $password = sha1('1234Test');

        $manager = new UserManager($entityManager);
        $user = $manager->get($email);

        $suffix = $user->getPassword()->getSuffix();

        $fullPassword = hash('sha256', $suffix.$password);

        if ($fullPassword === $user->getPassword()->getPassword()) {
            $this->session->set('auth', md5(uniqid(true)));

            return true;
        }

        return null;
    }
}
