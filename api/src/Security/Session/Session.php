<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Security\Session;

/**
 * Class Session.
 */
class Session
{
    /**
     * @var bool
     */
    public $autoSync = true;

    /**
     * @var string
     */
    private $token;

    /**
     * @var array
     */
    private $values = [];

    /**
     * Session constructor.
     */
    public function __construct()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    /**
     * @param string $key
     * @param $value
     */
    public function set(String $key, $value): void
    {
        $_SESSION[$key] = $value;
        $this->values[$key] = $value;
        $this->autoSyncSession();
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(String $key)
    {
        if (array_key_exists($key, $this->values)) {
            throw new \InvalidArgumentException(sprintf('Session key %s does not exist', $key));
        }

        return $this->values[$key];
    }

    public function crypt(): void
    {
        $this->token = bin2hex(json_encode($this));
        $this->autoSyncSession();
    }

    /**
     * TODO: Maybe could be auto synched with session/$_SESSION.
     *
     * @param string $token
     *
     * @return Session
     */
    public function read(String $token)
    {
        return json_decode(hex2bin($token));
    }

    public function sync(): void
    {
        if (isset($_SESSION)) {
            foreach ($_SESSION as $key => $val) {
                $this->values[$key] = $val;
            }

            foreach ($this->values as $valuesKey => $valuesVal) {
                $_SESSION[$valuesKey] = $valuesVal;
            }
        }
    }

    public function clear(): void
    {
        $this->values = [];
        if (isset($_SESSION)) {
            foreach ($_SESSION as $key => $val) {
                unset($_SESSION[$key]);
            }
        }
        $this->sync();
    }

    /**
     * @return Session
     */
    public function getFullSession(): self
    {
        $this->sync();
        $this->autoSync = null;

        return $this;
    }

    public function getSessionValues(): array
    {
        $this->sync();
        $this->autoSync = null;

        return $this->values;
    }

    private function autoSyncSession()
    {
        if (true === $this->autoSync) {
            $this->sync();
        }
    }
}
