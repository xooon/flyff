<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Manager\Page;

use App\Entity\Page;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PageManager.
 */
class PageManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * PageManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->pageRepository = $this->entityManager->getRepository(Page::class);
    }

    /**
     * @param string $name
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return null|Page
     */
    public function get(string $name): ?Page
    {
        $page = $this->pageRepository->findOneByName($name);

        if (!$page instanceof Page) {
            return null;
        }

        return $page;
    }
}
