<?php

/*
 * This file is part of Flyff CMS.
 *
 * (c) Xsrf <xenodev@outlook.de>
 *     Lejla <?>
 *     Jan <?>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Manager\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserManager.
 */
class UserManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * User constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $this->entityManager->getRepository(User::class);
    }

    /**
     * @param string $email
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return null|User
     */
    public function get(string $email): ?User
    {
        $user = $this->userRepository->findOneByEmail($email);

        if (!$user instanceof User) {
            return null;
        }

        return $user;
    }
}
