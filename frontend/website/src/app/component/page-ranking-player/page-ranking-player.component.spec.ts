import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRankingPlayerComponent } from './page-ranking-player.component';

describe('PageRankingPlayerComponent', () => {
  let component: PageRankingPlayerComponent;
  let fixture: ComponentFixture<PageRankingPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRankingPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRankingPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
