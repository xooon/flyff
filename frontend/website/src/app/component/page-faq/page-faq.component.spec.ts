import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageFAQComponent } from './page-faq.component';

describe('PageFAQComponent', () => {
  let component: PageFAQComponent;
  let fixture: ComponentFixture<PageFAQComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageFAQComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageFAQComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
