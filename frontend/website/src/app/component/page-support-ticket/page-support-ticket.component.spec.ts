import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSupportTicketComponent } from './page-support-ticket.component';

describe('PageSupportTicketComponent', () => {
  let component: PageSupportTicketComponent;
  let fixture: ComponentFixture<PageSupportTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageSupportTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSupportTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
