import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRankingGuildwarSingleComponent } from './page-ranking-guildwar-single.component';

describe('PageRankingGuildwarSingleComponent', () => {
  let component: PageRankingGuildwarSingleComponent;
  let fixture: ComponentFixture<PageRankingGuildwarSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRankingGuildwarSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRankingGuildwarSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
