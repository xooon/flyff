import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRankingGuildwarComponent } from './page-ranking-guildwar.component';

describe('PageRankingGuildwarComponent', () => {
  let component: PageRankingGuildwarComponent;
  let fixture: ComponentFixture<PageRankingGuildwarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRankingGuildwarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRankingGuildwarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
