import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUserInventoryComponent } from './page-user-inventory.component';

describe('PageUserInventoryComponent', () => {
  let component: PageUserInventoryComponent;
  let fixture: ComponentFixture<PageUserInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageUserInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageUserInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
