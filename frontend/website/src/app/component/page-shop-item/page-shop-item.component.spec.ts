import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageShopItemComponent } from './page-shop-item.component';

describe('PageShopItemComponent', () => {
  let component: PageShopItemComponent;
  let fixture: ComponentFixture<PageShopItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageShopItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageShopItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
