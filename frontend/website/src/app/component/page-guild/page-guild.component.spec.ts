import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGuildComponent } from './page-guild.component';

describe('PageGuildComponent', () => {
  let component: PageGuildComponent;
  let fixture: ComponentFixture<PageGuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
