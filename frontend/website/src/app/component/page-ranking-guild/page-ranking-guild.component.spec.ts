import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageRankingGuildComponent } from './page-ranking-guild.component';

describe('PageRankingGuildComponent', () => {
  let component: PageRankingGuildComponent;
  let fixture: ComponentFixture<PageRankingGuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageRankingGuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageRankingGuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
