import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGuildInventoryComponent } from './page-guild-inventory.component';

describe('PageGuildInventoryComponent', () => {
  let component: PageGuildInventoryComponent;
  let fixture: ComponentFixture<PageGuildInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGuildInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGuildInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
