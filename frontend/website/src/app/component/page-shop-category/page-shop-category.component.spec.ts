import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageShopCategoryComponent } from './page-shop-category.component';

describe('PageShopCategoryComponent', () => {
  let component: PageShopCategoryComponent;
  let fixture: ComponentFixture<PageShopCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageShopCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageShopCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
