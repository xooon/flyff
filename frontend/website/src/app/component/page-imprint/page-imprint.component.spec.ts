import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageImprintComponent } from './page-imprint.component';

describe('PageImprintComponent', () => {
  let component: PageImprintComponent;
  let fixture: ComponentFixture<PageImprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageImprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageImprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
