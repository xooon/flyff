import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageWikiComponent } from './page-wiki.component';

describe('PageWikiComponent', () => {
  let component: PageWikiComponent;
  let fixture: ComponentFixture<PageWikiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageWikiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageWikiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
