import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { PageHomeComponent } from './component/page-home/page-home.component';
import { PageRankingComponent } from './component/page-ranking/page-ranking.component';
import { PageRankingPlayerComponent } from './component/page-ranking-player/page-ranking-player.component';
import { PageRankingGuildComponent } from './component/page-ranking-guild/page-ranking-guild.component';
import { PageRankingGuildwarComponent } from './component/page-ranking-guildwar/page-ranking-guildwar.component';
import { PageRankingGuildwarSingleComponent } from './component/page-ranking-guildwar-single/page-ranking-guildwar-single.component';
import { PageShopComponent } from './component/page-shop/page-shop.component';
import { PageDonateComponent } from './component/page-donate/page-donate.component';
import { PageLoginComponent } from './component/page-login/page-login.component';
import { PageRegistrationComponent } from './component/page-registration/page-registration.component';
import { PageShopCategoryComponent } from './component/page-shop-category/page-shop-category.component';
import { PageShopItemComponent } from './component/page-shop-item/page-shop-item.component';
import { PageResetPasswordComponent } from './component/page-reset-password/page-reset-password.component';
import { PageFAQComponent } from './component/page-faq/page-faq.component';
import { PageImprintComponent } from './component/page-imprint/page-imprint.component';
import { PageAccountComponent } from './component/page-account/page-account.component';
import { PageGuildComponent } from './component/page-guild/page-guild.component';
import { PageUserComponent } from './component/page-user/page-user.component';
import { PageUserInventoryComponent } from './component/page-user-inventory/page-user-inventory.component';
import { PageGuildInventoryComponent } from './component/page-guild-inventory/page-guild-inventory.component';
import { PageWikiComponent } from './component/page-wiki/page-wiki.component';
import { PageDownloadComponent } from './component/page-download/page-download.component';
import { PageSupportComponent } from './component/page-support/page-support.component';
import { PageSupportTicketComponent } from './component/page-support-ticket/page-support-ticket.component';
import {FormsModule} from "@angular/forms";

const appRoutes: Routes = [
    { path: '', component: PageHomeComponent },
    { path: 'Ranking', component: PageRankingComponent },
    { path: 'Ranking/Player', component: PageRankingPlayerComponent },
    { path: 'Ranking/Guild', component: PageRankingGuildComponent },
    { path: 'Ranking/Guildwar', component: PageRankingGuildwarComponent },
    { path: 'Ranking/Guildwar/Single', component: PageRankingGuildwarSingleComponent },
    { path: 'Shop', component: PageShopComponent },
    { path: 'Shop/:categoryId', component: PageShopComponent },
    { path: 'Shop/:itemId', component: PageShopItemComponent },
    { path: 'Donate', component: PageDonateComponent },
    { path: 'Login', component: PageLoginComponent },
    { path: 'Registration', component: PageRegistrationComponent },
    { path: 'Password/Reset', component: PageResetPasswordComponent },
    { path: 'FAQ', component: PageFAQComponent },
    { path: 'Imprint', component: PageImprintComponent },
    { path: 'Account', component: PageAccountComponent },
    { path: 'Guild', component: PageGuildComponent },
    { path: 'Guild/Inventory', component: PageGuildInventoryComponent },
    { path: 'User', component: PageUserComponent },
    { path: 'User/Inventory', component: PageUserInventoryComponent },
    { path: 'Wiki', component: PageWikiComponent },
    { path: 'Download', component: PageDownloadComponent },
    { path: 'Support', component: PageSupportComponent },
    { path: 'Support/Ticket', component: PageSupportTicketComponent },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    PageHomeComponent,
    PageRankingComponent,
    PageRankingPlayerComponent,
    PageRankingGuildComponent,
    PageRankingGuildwarComponent,
    PageRankingGuildwarSingleComponent,
    PageShopComponent,
    PageDonateComponent,
    PageLoginComponent,
    PageRegistrationComponent,
    PageShopCategoryComponent,
    PageShopItemComponent,
    PageResetPasswordComponent,
    PageFAQComponent,
    PageImprintComponent,
    PageAccountComponent,
    PageGuildComponent,
    PageUserComponent,
    PageUserInventoryComponent,
    PageGuildInventoryComponent,
    PageWikiComponent,
    PageDownloadComponent,
    PageSupportComponent,
    PageSupportTicketComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
        appRoutes,
        { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
